package com.pawelbanasik;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
	private String name;
	private String artist;
	private SongList songList;

	public Album(String name, String artist) {
		this.name = name;
		this.artist = artist;
		this.songList = new SongList();

	}
	
	public boolean addSong(String title, double duration) {
			songList.add(new Song(title, duration));
			return true;

	}
	

	public boolean addToPlayList(int trackNumber, LinkedList<Song> playList) {
		int index = trackNumber - 1;
		if ((index > 0) && (index <= this.songList.getSongs().size())) {
			playList.add(this.songList.getSongs().get(index));
			return true;
		}
		System.out.println("This album does not have a track " + trackNumber);
		return false;
	}

	public boolean addToPlayList(String title, LinkedList<Song> playList) {
		Song checkedSong = songList.findSong(title);
		if (checkedSong != null) {
			playList.add(checkedSong);
			return true;
		}
		System.out.println("The song " + title + " is not in this album");
		return false;
	}

	private class SongList {
		private ArrayList<Song> songs;

		protected SongList() {
			this.songs = new ArrayList<Song>();
		}

		public boolean add(Song song) {
			if (songs.contains(song)) {
				return false;
			}
			songs.add(song);
			return true;
		}

		private Song findSong(String title) {
			for (Song checkedSong : this.songs) {
				if (checkedSong.getTitle().equals(title)) {
					return checkedSong;
				}
			}
			return null;
		}

		public ArrayList<Song> getSongs() {
			return songs;
		}

		
		
	}

}
